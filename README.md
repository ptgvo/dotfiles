# Description

A compendium of my configuration files for MacOS and Linux.

# Features

+ editor
  + __neovim__
+ window manager(s)
  + **hammerspoon**
  + **skhd** (hotkeys)
+ shell
  + __zsh__
  + __oh-my-zsh__ (plugin manager)
+ terminal
  + __iterm__
  + __tmux__
+ other
  + __git__

# To install

```sh
$ git clone git@github.com:philliptvo/dotfiles.git
$ cd dotfiles
$ ./setup.sh
```

# Post Installation

## Install plugins

WIP

## Setup LSP

*WIP*
