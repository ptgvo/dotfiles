#!/bin/sh

REMOVE_FLAG=0

base=(
  zsh
  bin
)

user=(
  # chemacs
  # doom
  git
  nvim
  python
  luaformatter
  tmux
)

mac=(
  hammerspoon
)

linux=(
  # x
  # fontconfig
  # picom
  # zathura
)

stowit() {
  # -v verbose
  # -R relink
  # -D delete link
  # -t target

  usr=$1
  app=$2
  [ $REMOVE_FLAG -eq 0 ] \
    && stow -v -R -t "${usr}" "${app}" \
    || stow -v -D -t "${usr}" "${app}"
}

installonmac() {
  for app in ${mac[@]}; do
    stowit "${HOME}" "${app}"
  done
}


installonlinux() {
  for app in ${linux[@]}; do
    stowit "${HOME}" "${app}"
  done
}

installextra() {
  [ "$(uname)" == "Darwin" ] && installonmac
  [ "$(uname)" == "Linux" ] && installonlinux
}

while [ $# -gt 0 ]; do
  cur=$1
  shift

  case "$cur" in
    "-D")
      REMOVE_FLAG=1
      ;;
    *)
      echo "Unavailable command... $curr"
      exit 1
  esac
done

# ---- Script starts here ----
# install apps available to local users and root
for app in ${base[@]}; do
  stowit "${HOME}" "${app}"
done

# install user-only apps
for app in ${user[@]}; do
  stowit "${HOME}" "${app}"
done

# install distro-specific apps
installextra

echo "=================================================="
echo "Next Steps:"
echo "  ..."
