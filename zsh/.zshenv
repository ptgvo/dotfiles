#!/usr/bin/env zsh

# Clean $HOME up
export XDG_CACHE_HOME="${XDG_CACHE_HOME:=${HOME}/.cache}"
export XDG_DATA_HOME="${XDG_DATA_HOME:=${HOME}/.local/share}"
export XDG_BIN_HOME="${XDG_BIN_HOME:=${HOME}/.local/bin}"
export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:=${HOME}/.config}"

export EDITOR="vi"

# Zsh config
export ZDOTDIR="${XDG_CONFIG_HOME}/zsh"
export HISTFILE="${ZDOTDIR}/.zhistory"  # History filepath
export HISTSIZE=10000                   # Maximum events for internal history
export SAVEHIST=10000                   # Maximum events in history file
export ZSH="${XDG_CONFIG_HOME}/oh-my-zsh"

# less
export LESSHISTFILE="-"

# x11
export XINITRC="${XDG_CONFIG_HOME:=${HOME}/.config}/x/xinitrc"
export XAUTHORITY="${XDG_RUNTIME_DIR}"/Xauthority

# rust
export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export CARGO_BIN="${XDG_DATA_HOME}/cargo/bin"

# golang
export GO_PATH="${XDG_DATA_HOME}/go"
export GO_BIN="${XDG_DATA_HOME}/go/bin"
export GO_CACHE="${XDG_CACHE_HOME}/go-build"

# npm
export NPM_PATH="${XDG_CONFIG_HOME}/node_modules"
export NPM_BIN="${XDG_CONFIG_HOME}/node_modules/bin"

# haskell
export GHCUP_USE_XDG_DIRS=true

# python
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc"

# anaconda
export CONDA_ROOT="${XDG_CONFIG_HOME}/conda"

# jupyter-notebook
export JUPYTER_CONFIG_PATH="#{XDG_CONFIG_HOME}/jupyter"

# DEPRECATE
export PYENV_ROOT="${XDG_DATA_HOME}/pyenv"
export GRADLE_USER_HOME="${XDG_DATA_HOME}/gradle"

# [ "$(uname -s)" = "Darwin" ] && export PATH="/usr/local/opt/llvm/bin:${PATH}"
# which brew &>/dev/null 2>&1 && export GOROOT="$(brew --prefix golang)/libexec"

# PATH
export PATH="${XDG_BIN_PATH}:${PATH}"                           # user binaries
export PATH="${HOME}/.local/bin/scripts:${PATH}"                # shell scripts
export PATH="/usr/local/sbin:${PATH}"                           # homebrew
export PATH="${CARGO_BIN}:${PATH}"                              # rust
export PATH="${GO_BIN}:${PATH}"                                 # golang
export PATH="${PYENV_ROOT}/bin:${PATH}"                         # pyenv
export PATH="${NPM_BIN}:${PATH}"                                # npm
export PATH="${HOME}/.config/emacs.doom/bin:${PATH}"            # doom (remove)
export PATH="/usr/local/opt/coreutils/libexec/gnubin:${PATH}"   # gnu utils
export PATH="/usr/local/opt/binutils/bin:${PATH}"               # bin utils

# MANPATH
export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:${MANPATH}"
