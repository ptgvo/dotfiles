;;; profiles.el --- Chemacs Profiles
;;; Commentary:
;;;     Entry point to for chemacs; multiple Emacs configurations
;;; Code:

(("default" . ((user-emacs-directory . "~/.config/emacs.gnu")
               (server-name . "gnu")
	       ))

 ("doom" . ((user-emacs-directory . "~/.config/emacs.doom")
	    ))
 )
