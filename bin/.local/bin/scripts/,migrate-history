#!/usr/bin/env python3
"""
Convert fish history to zsh history

This script takes the fish shell history and converts it to be used in the zsh
shell. Script is modified for python3 from here:

https://gist.github.com/mateuspontes/f6c26c94ca87eaa49cc977f33043405f
"""

import os
import re

"""fish to zsh helper function

Converts multi-line commands in the history.

Arguments
---------
cmd: String
    the command to convert

Returns
-------
the command adapted to zsh history format
"""
def fish_to_zsh(cmd: str) -> str:
    return (cmd.replace('; and ', '&&')
               .replace('; or ', '||'))

# ---- Main Script ----
with open(os.path.expanduser('~/.zsh_history'), 'a') as o:
    with open(os.path.expanduser('~/.local/share/fish/fish_history')) as f:
        for line in f:
            line: str = line.strip()
            if line and re.match('^- cmd:', line):
                meta, command = line.split('- cmd: ', 1)
                line: str = f.readline().strip()
                if line and re.match('^when:', line):
                    meta, when = line.split('when: ', 1)
                    o.write(': %s:0;%s\n' % (when, fish_to_zsh(command)))
